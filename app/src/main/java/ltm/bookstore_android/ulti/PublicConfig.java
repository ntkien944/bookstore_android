package ltm.bookstore_android.ulti;

import ltm.bookstore_android.sqlite.BooksDataSource;
import ltm.bookstore_android.sqlite.CartsDataSource;
import ltm.bookstore_android.sqlite.CategoriesDataSource;
import ltm.bookstore_android.sqlite.UsersDataSource;

/**
 * Created by Administrator on 6/19/2015.
 */

/**
 * TODO: dua cac info nay vao trong xml
 *
 */
public class PublicConfig {

    /* DEBUGGING CONFIG*/
    public static final String TAG_SQLITE = "SQLITE";
    public static final String TAG_ACTIVITY = "ACTIVITY";

    /* DATABASE CONFIG*/
    public static final String DATABASE_NAME = "dbookstorev7.db";
    public static final int DATABASE_VERSION = 7; // Them phan book_url cho table BOOK



    /** CATEGORY TABLE **/
    public static final String TABLE_CATEGORY = "category";
    public static final String COLUMN_CATEGORY_ID = "category_id";
    public static final String COLUMN_CATEGORY_NAME = "category_name";

    /** BOOK TABLE **/
    public static final String TABLE_BOOK = "book";
    public static final String COLUMN_BOOK_ID = "book_id";
    public static final String COLUMN_BOOK_NAME = "book_name";
    public static final String COLUMN_BOOK_AUTHOR = "book_author";
    public static final String COLUMN_BOOK_PUBLISHER = "book_publisher";
    public static final String COLUMN_BOOK_PRICE = "book_price";
    public static final String COLUMN_BOOK_CATEGORY_ID = "category_id";
    public static final String COLUMN_BOOK_URL = "book_url";

    /** USER TABLE **/
    public static final String TABLE_USER = "user";
    public static final String COLUMN_USER_NAME = "username";
    public static final String COLUMN_USER_PASSWORD = "password";
    public static final String COLUMN_USER_EMAIL = "email";
    public static final String COLUMN_USER_ADDRESS = "address";
    public static final String COLUMN_USER_PHONE = "phone";

    /** CART TABLE **/
    public static final String TABLE_CART = "cart";
    public static final String COLUMN_CART_USERNAME = "username";
    public static final String COLUMN_CART_BOOKID  = "book_id";

    /*** Dont touch these. ***/
    public static CategoriesDataSource categoriesDataSource = null;
    public static BooksDataSource booksDataSource = null;
    public static UsersDataSource usersDataSource = null;
    public static CartsDataSource cartsDataSource = null;

}
