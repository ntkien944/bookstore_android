package ltm.bookstore_android;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import ltm.bookstore_android.sqlite.MySQLiteHelper;

import static ltm.bookstore_android.ulti.PublicInfo.USERNAME;


/**
 * Created by Administrator on 6/24/2015.
 */
public class MainActivity extends Activity{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        /***** DATABASE INIT***/
        MySQLiteHelper.initializeSQLiteDatabase(this); // Create datasource instances for table and

        /* END DATABASE INIT***/

        // TODO: set content
        Intent i = new Intent(getApplicationContext(), LoginPageActivity.class);
        startActivity(i);
    }
}
