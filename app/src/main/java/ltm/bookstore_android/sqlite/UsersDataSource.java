package ltm.bookstore_android.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import java.util.ArrayList;
import java.util.List;
import ltm.bookstore_android.model.pojo.User;
import ltm.bookstore_android.ulti.PublicConfig;
import android.util.Log;
import static ltm.bookstore_android.ulti.PublicConfig.*;

public class UsersDataSource extends DataSource{

    // Database fields

    public UsersDataSource(Context context) {
        super(context);
        this.allColumns = new String[]{COLUMN_USER_NAME,
            COLUMN_USER_PASSWORD, COLUMN_USER_EMAIL,
            COLUMN_USER_ADDRESS, COLUMN_USER_PHONE};
    }

    public User createUser(String userName, String password, String email
                , String address, String phone) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_USER_NAME, userName);
        values.put(COLUMN_USER_PASSWORD, password);
        values.put(COLUMN_USER_EMAIL, email);
        values.put(COLUMN_USER_ADDRESS, address);
        values.put(COLUMN_USER_PHONE, phone);

        database.insertWithOnConflict(TABLE_USER, null,
                values, SQLiteDatabase.CONFLICT_IGNORE);
        Cursor cursor = database.query(TABLE_USER,
                allColumns, COLUMN_USER_NAME + "=" + "\'" + userName + "\'", null,
                null, null, null);
        cursor.moveToFirst();
        User newUser = cursorToUser(cursor);
        cursor.close();
        return newUser;
    }

    public void deleteUser(String userName) {
        database.delete(TABLE_USER, COLUMN_USER_NAME
                + "=" + "\'" + userName + "\'", null);
    }

    public boolean updateUser(String userName, String password, String email
            , String address, String phone) {
        ContentValues args = new ContentValues();

        args.put(COLUMN_USER_NAME, userName);
        args.put(COLUMN_USER_PASSWORD, password);
        args.put(COLUMN_USER_EMAIL, email);
        args.put(COLUMN_USER_PHONE, phone);
        args.put(COLUMN_USER_ADDRESS, address);

        return database.update(TABLE_USER, args, COLUMN_USER_NAME + "=" + "\'" + userName + "\'", null) > 0;
    }


    public User getUser(String userName) {
        Cursor cursor = database.query(TABLE_USER,
                allColumns, COLUMN_USER_NAME + "=" + "\'" + userName + "\'", null,
                null, null, null);
        cursor.moveToFirst();
        User newUser = cursorToUser(cursor);
        cursor.close();
        return newUser;
    }

    public List<User> getAllUsers() {
        List<User> users = new ArrayList<User>();

        Cursor cursor = database.query(TABLE_USER,
                allColumns, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            User user = cursorToUser(cursor);
            users.add(user);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return users;
    }


    private User cursorToUser(Cursor cursor) {

        User user = new User();
        user.setUserName(cursor.getString(0));
        user.setPassword(cursor.getString(1));
        user.setEmail(cursor.getString(2));
        user.setAddress(cursor.getString(3));
        user.setPhone(cursor.getString(4));

        return user;
    }

    public void showUser(User user) {

        Log.d(PublicConfig.TAG_SQLITE, user.toString());
    }

    public void showAllUsers() {
        Log.d(PublicConfig.TAG_SQLITE, "User list:");
        List<User> users = getAllUsers();
        for(int i = 0; i < users.size(); i++) {
            showUser(users.get(i));
        }
    }

    /**
     *
     * TODO: check ham isContain()
     */
    public boolean isContain(String userName) {
        Cursor cursor = database.query(TABLE_USER,
                allColumns, COLUMN_USER_NAME + "=" + "\'" + userName + "\'", null,
                null, null, null);
        cursor.moveToFirst();
        if(cursor.getCount() == 0) {
            return false;
        }
        cursor.close();


        return true;
    }

    public boolean isContain(String userName, String password) {
        Cursor cursor = database.query(TABLE_USER,
                allColumns, COLUMN_USER_NAME + "=" + "\'" + userName + "\'"
                        + "AND " + COLUMN_USER_PASSWORD + " = " + password, null,
                null, null, null);

        cursor.moveToFirst();

        if(cursor.getCount() == 1) {
            return true;
        }
        cursor.close();


        return false;
    }

}


