package ltm.bookstore_android.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import ltm.bookstore_android.model.pojo.Cart;
import ltm.bookstore_android.ulti.PublicConfig;

import static ltm.bookstore_android.ulti.PublicConfig.*;

public class CartsDataSource extends DataSource{

    public CartsDataSource(Context context) {
        super(context);
        this.allColumns = new String[]{COLUMN_CART_USERNAME, COLUMN_CART_BOOKID};
    }

    public Cart createCart(String userName, int bookId) {
        ContentValues values = new ContentValues();

        /********* ALL FIELD ***********/
        values.put(COLUMN_CART_USERNAME, userName);
        values.put(COLUMN_CART_BOOKID, bookId);
        /********* ALL FIELD ***********/

        database.insertWithOnConflict(TABLE_CART, null,
                values, SQLiteDatabase.CONFLICT_IGNORE);
        Cursor cursor = database.query(TABLE_CART,
                allColumns, COLUMN_CART_USERNAME + " = " + "\'" + userName + "\'"
                        + "AND " + COLUMN_CART_BOOKID + " = " + bookId, null,
                null, null, null);
        cursor.moveToFirst();
        Cart newCart = cursorToCart(cursor);
        cursor.close();
        Log.d(TAG_SQLITE + " "  + getClass().toString(), newCart.toString());
        return newCart;
    }

    public void deleteCart(String userName) {
        database.delete(TABLE_CART, COLUMN_CART_USERNAME + " = " + "\'" + userName + "\'", null);
    }



    public List<Cart> getALlCartsOfUser(String userName) {
        List<Cart> carts = new ArrayList<Cart>();

        Cursor cursor = database.query(TABLE_CART,
                allColumns, COLUMN_CART_USERNAME + " = " + "\'" + userName + "\'",
                null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Cart cart = cursorToCart(cursor);
            carts.add(cart);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return carts;
    }

    public List<Cart> getAllCarts() {
        List<Cart> carts = new ArrayList<Cart>();

        Cursor cursor = database.query(TABLE_CART,
                allColumns, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Cart cart = cursorToCart(cursor);
            carts.add(cart);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return carts;
    }


    private Cart cursorToCart(Cursor cursor) {

        Cart cart = new Cart();
        cart.setUserName(cursor.getString(0));
        cart.setBookId(cursor.getInt(1));

        return cart;
    }

    public void showCart(Cart cart) {
        Log.d(PublicConfig.TAG_SQLITE, cart.toString());
    }

    public void showAllCarts() {
        Log.d(PublicConfig.TAG_SQLITE, "Cart list:");
        List<Cart> carts = getAllCarts();
        for(int i = 0; i < carts.size(); i++) {
            showCart(carts.get(i));
        }


    }
}
