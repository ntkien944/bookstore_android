package ltm.bookstore_android.sqlite;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by Administrator on 6/19/2015.
 */
public class DataSource {
    protected SQLiteDatabase database;
    protected MySQLiteHelper dbHelper;
    protected String[] allColumns = {};

    public DataSource(Context context) {
        dbHelper = new MySQLiteHelper(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }
}
