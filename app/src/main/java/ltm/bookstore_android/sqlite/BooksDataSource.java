package ltm.bookstore_android.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import ltm.bookstore_android.model.pojo.Book;
import ltm.bookstore_android.model.pojo.Category;
import ltm.bookstore_android.ulti.PublicConfig;

import static ltm.bookstore_android.ulti.PublicConfig.*;

public class BooksDataSource extends DataSource{

    // Database fields



    public BooksDataSource(Context context) {
        super(context);
        this.allColumns = new String[]{COLUMN_BOOK_ID, COLUMN_BOOK_NAME
            , COLUMN_BOOK_AUTHOR, COLUMN_BOOK_PUBLISHER, COLUMN_BOOK_CATEGORY_ID
            , COLUMN_BOOK_PRICE, COLUMN_BOOK_URL};
    }

    public Book createBook(int id, String name,
                            String author, String publisher,
                            int bookCategoryId, int price, String bookUrl) {
        ContentValues values = new ContentValues();

        /********* ALL FIELD ***********/
        values.put(COLUMN_BOOK_ID, id);
        values.put(COLUMN_BOOK_NAME, name);
        values.put(COLUMN_BOOK_AUTHOR, author);
        values.put(COLUMN_BOOK_PUBLISHER, publisher);
        values.put(COLUMN_BOOK_CATEGORY_ID, bookCategoryId);
        values.put(COLUMN_BOOK_PRICE, price);
        values.put(COLUMN_BOOK_URL, bookUrl);
        /********* ALL FIELD ***********/

        database.insertWithOnConflict(TABLE_BOOK, null,
                values, SQLiteDatabase.CONFLICT_IGNORE);
        Cursor cursor = database.query(TABLE_BOOK,
                allColumns, COLUMN_BOOK_ID + " = " + id, null,
                null, null, null);
        cursor.moveToFirst();
        Book newBook = cursorToBook(cursor);
        cursor.close();
        return newBook;
    }

    public void deleteBook(int id) {
        database.delete(TABLE_BOOK, COLUMN_BOOK_ID
                + " = " + id, null);
    }

    public boolean updateBook(int id, String name,
                              String author, String publisher,
                              int bookCategoryId, int price, String bookUrl) {
        ContentValues args = new ContentValues();
        args.put(COLUMN_BOOK_ID, id);
        args.put(COLUMN_BOOK_NAME, name);
        args.put(COLUMN_BOOK_ID, id);
        args.put(COLUMN_BOOK_NAME, name);
        args.put(COLUMN_BOOK_AUTHOR, author);
        args.put(COLUMN_BOOK_PUBLISHER, publisher);
        args.put(COLUMN_BOOK_CATEGORY_ID, bookCategoryId);
        args.put(COLUMN_BOOK_PRICE, price);
        args.put(COLUMN_BOOK_URL, bookUrl);

        return database.update(TABLE_BOOK, args, COLUMN_BOOK_ID + "=" + id, null) > 0;
    }

    public List<Book> getAllBooks() {
        List<Book> books = new ArrayList<Book>();

        Cursor cursor = database.query(TABLE_BOOK,
                allColumns, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Book book = cursorToBook(cursor);
            books.add(book);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return books;
    }

    public List<Book> getAllBooksInCategory(int categoryId) {
        List<Book> books = new ArrayList<Book>();
        Cursor cursor = null;
        if(categoryId == Category.CATEGORY_ID_ALL_BOOKS) { // GET ALL BOOKS
            cursor = database.query(TABLE_BOOK,
                    allColumns, null, null, null, null, null);
        }
        else { // GET all books in category with category id
            cursor = database.query(TABLE_BOOK,
                    allColumns, COLUMN_BOOK_CATEGORY_ID + "=" + categoryId, null, null, null, null);
        }


        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Book book = cursorToBook(cursor);
            books.add(book);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return books;
    }


    private Book cursorToBook(Cursor cursor) {

        Book book = new Book();
        book.setBookId(cursor.getInt(0));
        book.setBookName(cursor.getString(1));
        book.setBookAuthor(cursor.getString(2));
        book.setBookPublisher(cursor.getString(3));
        book.setBookCategoryId(cursor.getInt(4));
        book.setBookPrice(cursor.getInt(5));
        book.setBookUrl(cursor.getString(6));

        return book;
    }

    public void showBook(Book book) {

        Log.d(PublicConfig.TAG_SQLITE, book.toString());
    }

    public void showAllBooks() {
        Log.d(PublicConfig.TAG_SQLITE, "Book list:");
        List<Book> books = getAllBooks();
        for(int i = 0; i < books.size(); i++) {
            showBook(books.get(i));
        }
    }

    public Book getBook(int bookId) {
        Cursor cursor = database.query(TABLE_BOOK,
                allColumns, COLUMN_BOOK_ID + " = " + bookId, null,
                null, null, null);
        cursor.moveToFirst();
        Book newBook = cursorToBook(cursor);
        cursor.close();

        return newBook;
    }



}


