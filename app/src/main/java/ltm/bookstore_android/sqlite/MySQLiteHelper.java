package ltm.bookstore_android.sqlite;

import android.content.Context;
import android.content.res.AssetManager;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import static ltm.bookstore_android.ulti.PublicConfig.*;

import ltm.bookstore_android.ulti.PublicConfig;

public class MySQLiteHelper extends SQLiteOpenHelper {

    private final Context context;

    public MySQLiteHelper(Context context) {

       // super(context, DATABASE_NAME, null, DATABASE_VERSION);
        super(context, Environment.getExternalStorageDirectory().getPath() + File.separator +
                        DATABASE_NAME,
                null, DATABASE_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase database) {

        try {
            executeSQLScript(database, "scripts/create_table_category.sql");
            executeSQLScript(database, "scripts/create_table_book.sql");
            executeSQLScript(database, "scripts/create_table_user.sql");
            executeSQLScript(database, "scripts/create_table_cart.sql");
        } catch (IOException e) {
            Log.d(PublicConfig.TAG_SQLITE, "Failed to execute sql script. ");
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(PublicConfig.TAG_SQLITE,
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CATEGORY);
        Log.w(PublicConfig.TAG_SQLITE, "Drop table category");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_BOOK);
        Log.w(PublicConfig.TAG_SQLITE, "Drop table book");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER);
        Log.w(PublicConfig.TAG_SQLITE, "Drop table user");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CART);
        Log.w(PublicConfig.TAG_SQLITE, "Drop table cart");

        onCreate(db);
    }

    private void executeSQLScript(SQLiteDatabase database,  String dbname) throws IOException {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        byte buf[] = new byte[1024];
        int len;
        AssetManager assetManager = context.getAssets();
        InputStream inputStream = null;

        try{
            inputStream = assetManager.open(dbname);
            while ((len = inputStream.read(buf)) != -1) {
                outputStream.write(buf, 0, len);
            }
            outputStream.close();
            inputStream.close();

            String[] createScript = outputStream.toString().split(";");
            for (int i = 0; i < createScript.length; i++) {
                String sqlStatement = createScript[i].trim();

                if (sqlStatement.length() > 0) {
                    database.execSQL(sqlStatement + ";");
                }
            }
        } catch (IOException e){
            Log.d(PublicConfig.TAG_SQLITE, "Input Stream error.");
        } catch (SQLException e) {
            Log.d(PublicConfig.TAG_SQLITE, "SQL exception throws.");
        }
    }

    public static void initializeSQLiteDatabase(Context context){
        categoriesDataSource = new CategoriesDataSource(context);
        booksDataSource = new BooksDataSource(context);
        usersDataSource = new UsersDataSource(context);
        cartsDataSource = new CartsDataSource(context);

        categoriesDataSource.open();
        categoriesDataSource.createCategory(1, "Van hoc");
        categoriesDataSource.createCategory(2, "Giao duc");
        categoriesDataSource.createCategory(3, "Khoa hoc");
        categoriesDataSource.createCategory(4, "Xa hoi");
        categoriesDataSource.createCategory(5, "Thieu nhi");
        categoriesDataSource.showAllCategories();
        categoriesDataSource.close();


        booksDataSource.open();

        booksDataSource.createBook(1, "Chi Dau", "Ngo Tat to", "Nha xuat ban van hoc", 1, 60000, "http://ditichlichsuvanhoa.com/ck/upload/images/tat%20den.jpg");
        booksDataSource.createBook(2, "Doremon Tap 1", "Fuji", "Nha xuat ban Kim Dong", 5, 18000,"http://data2.manga24h.com/manga/doremon/12/1.jpg?imgmax=3000");
        booksDataSource.createBook(3, "The Adventures of Sherlock Holmes", "Sir Arthur Conan Doyle", "Amazon", 1, 300000,"https://justzip.files.wordpress.com/2010/01/sherlock-holmes-poster1.jpg");
        booksDataSource.createBook(4, "The Odyssey", "Homer", "Amazon", 1, 400000, "http://ecx.images-amazon.com/images/I/51teQS2zYBL._SY344_BO1,204,203,200_.jpg");
        booksDataSource.createBook(5, "Toan 4", "Hoi dong giao duc", "Nha xuat ban giao duc", 2, 18000, "http://1.bp.blogspot.com/-Ov8ZDzdhPoI/U66VgbHDS_I/AAAAAAAACL4/aZtzKSH6oc8/s1600/gia+su+toan+4.jpg");

        booksDataSource.showAllBooks();
        booksDataSource.close();

        usersDataSource.open();
        usersDataSource.createUser("ntkien944", "123456", "ntkien944@gmail.com", "Ho chi minh city", "09423477");
        usersDataSource.showAllUsers();

        usersDataSource.close();

    }
}