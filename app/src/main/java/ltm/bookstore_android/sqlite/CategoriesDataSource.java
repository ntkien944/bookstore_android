package ltm.bookstore_android.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;


import ltm.bookstore_android.model.pojo.Category;
import ltm.bookstore_android.ulti.PublicConfig;

import android.util.Log;
import static ltm.bookstore_android.ulti.PublicConfig.*;

public class CategoriesDataSource extends DataSource{

    // Database fields

    public CategoriesDataSource(Context context) {
        super(context);
        this.allColumns = new String[]{COLUMN_CATEGORY_ID, COLUMN_CATEGORY_NAME};
    }

    public Category createCategory(int id, String name) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_CATEGORY_ID, id);
        values.put(COLUMN_CATEGORY_NAME, name);

        database.insertWithOnConflict(TABLE_CATEGORY, null,
                values, SQLiteDatabase.CONFLICT_IGNORE);
        Cursor cursor = database.query(TABLE_CATEGORY,
                allColumns, COLUMN_CATEGORY_ID + " = " + id, null,
                null, null, null);
        cursor.moveToFirst();
        Category newCategory = cursorToCategory(cursor);
        cursor.close();
        return newCategory;
    }

    public void deleteCategory(int id) {
        database.delete(TABLE_CATEGORY, COLUMN_CATEGORY_ID
                + " = " + id, null);
    }

    public boolean updateCategory(int id, String name) {
        ContentValues args = new ContentValues();
        args.put(COLUMN_CATEGORY_ID, id);
        args.put(COLUMN_CATEGORY_NAME, name);
        return database.update(TABLE_CATEGORY, args, COLUMN_CATEGORY_ID + "=" + id, null) > 0;
    }

    public Category getCategory(int id) {
        Cursor cursor = database.query(TABLE_CATEGORY,
                allColumns, COLUMN_CATEGORY_ID + " = " + id, null,
                null, null, null);
        cursor.moveToFirst();
        Category newCategory = cursorToCategory(cursor);
        cursor.close();
        return newCategory;
    }

    public List<Category> getAllCategories() {
        List<Category> categories = new ArrayList<Category>();

        Cursor cursor = database.query(TABLE_CATEGORY,
                allColumns, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Category category = cursorToCategory(cursor);
            categories.add(category);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return categories;
    }


    private Category cursorToCategory(Cursor cursor) {

        Category category = new Category();
        category.setCategoryId(cursor.getInt(0));
        category.setCategoryName(cursor.getString(1));

        return category;
    }

    public void showCategory(Category category) {
        Log.d(PublicConfig.TAG_SQLITE, category.toString());
    }

    public void showAllCategories() {
        Log.d(PublicConfig.TAG_SQLITE, "Category list:");
        List<Category> categories = getAllCategories();
        for(int i = 0; i < categories.size(); i++) {
            showCategory(categories.get(i));
        }
    }

}


