package ltm.bookstore_android;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import ltm.bookstore_android.controller.SimpleBookListAdapter;
import ltm.bookstore_android.model.pojo.Book;
import ltm.bookstore_android.model.pojo.Cart;
import ltm.bookstore_android.ulti.PublicInfo;

import static ltm.bookstore_android.ulti.PublicConfig.*;

/**
 * Created by Administrator on 6/21/2015.
 */
public class CartActivity extends Activity{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_cart_page);

        /* Lay sach tu gio hang cua user dang xai app. */
        cartsDataSource.open();

        /**
         * TODO: Check ham getAllCartsOfUser.
         */
        List<Cart> listCart = cartsDataSource.getALlCartsOfUser(PublicInfo.USERNAME);
        cartsDataSource.close();



        List<Book> listBook = new ArrayList<Book>();
        if(listCart.size() != 0) {
            booksDataSource.open();
            for(int i = 0; i < listCart.size(); i++) {
                Book book = booksDataSource.getBook(listCart.get(i).getBookId());
                listBook.add(book);
            }
            booksDataSource.close();



            final GridView basicBookListView = (GridView) findViewById(R.id.cartpage_list_book);
            SimpleBookListAdapter simpleBookListAdapter = new SimpleBookListAdapter(this, listBook);

            basicBookListView.setAdapter(simpleBookListAdapter);

            basicBookListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                }
            });
        }
        else
        {
            Toast.makeText(this, "There is no book in your cart! ", Toast.LENGTH_LONG).show();
            Toast.makeText(this, "Please go back to store and add one!", Toast.LENGTH_LONG).show();
        }


        /* Implement back to store button */
        Button buttonBackToStore = (Button) findViewById(R.id.cartpage_button_back_to_store);
        buttonBackToStore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), MainPageActivity.class);
                startActivity(i);
            }
        });

        /**
         * TODO: Implement purchase button
         */


    }
}
