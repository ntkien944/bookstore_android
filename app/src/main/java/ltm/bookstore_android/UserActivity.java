package ltm.bookstore_android;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import ltm.bookstore_android.model.pojo.User;

import static ltm.bookstore_android.ulti.PublicConfig.*;
import static ltm.bookstore_android.ulti.PublicInfo.*;

/**
 * Created by Administrator on 6/17/2015.
 */
public class UserActivity extends Activity{
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);


        /* Back button. */
        ImageButton backButton = (ImageButton) findViewById(R.id.userpage_back_button);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), MainPageActivity.class);
                startActivity(i);
            }
        });

        /* Get info view */
        TextView usernameView = (TextView) findViewById(R.id.userpage_username);
        TextView emailView = (TextView) findViewById(R.id.userpage_email);
        TextView addressView = (TextView) findViewById(R.id.userpage_address);
        TextView phoneView = (TextView) findViewById(R.id.userpage_phone);


        /* Set user info to view. */
        usersDataSource.open();
        final User user = usersDataSource.getUser(USERNAME);
        usersDataSource.close();
        usernameView.setText(user.getUserName());
        emailView.setText(user.getEmail());
        addressView.setText(user.getAddress());
        phoneView.setText(user.getPhone());

        /* Get change info view. */
        final EditText newPasswordView = (EditText) findViewById(R.id.userpage_newpassword_text);
        final EditText newEmailView = (EditText) findViewById(R.id.userpage_newemail_text);
        final EditText newAddressView = (EditText) findViewById(R.id.userpage_newaddress_text);
        final EditText newPhoneView = (EditText) findViewById(R.id.userpage_newphone_text);

        Button newPasswordButton = (Button) findViewById(R.id.userpage_newpassword_button);
        Button newEmailButton = (Button) findViewById(R.id.userpage_newemail_button);
        Button newAddressButton = (Button) findViewById(R.id.userpage_newaddress_button);
        Button newPhoneButton = (Button) findViewById(R.id.userpage_newphone_button);

        /* Handle change info event */

        newPasswordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /* Update password in database. */
                String newPassword = newPasswordView.getText().toString();

                usersDataSource.open();
                if(newPassword.equals(user.getPassword())) {// Match old password
                    Toast.makeText(getApplicationContext(), "Error: New password = old password.", Toast.LENGTH_SHORT).show();
                }
                else { // Valid new password
                    usersDataSource.updateUser(user.getUserName(), newPassword, user.getEmail(),
                            user.getAddress(), user.getPhone());
                    Toast.makeText(getApplicationContext(), "Change password successfully.", Toast.LENGTH_SHORT).show();

                }
                usersDataSource.close();
            }
        });


        newEmailButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /* Update email in database. */
                String newEmail = newEmailView.getText().toString();

                usersDataSource.open();
                usersDataSource.updateUser(user.getUserName(), user.getPassword(), newEmail,
                        user.getAddress(), user.getPhone());
                Toast.makeText(getApplicationContext(), "Change email successfully.", Toast.LENGTH_SHORT).show();
                usersDataSource.close();

                Intent i = new Intent(getApplicationContext(), UserActivity.class);
                startActivity(i);
            }
        });

        newAddressButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /* Update email in database. */
                String newAddress = newAddressView.getText().toString();

                usersDataSource.open();
                usersDataSource.updateUser(user.getUserName(), user.getPassword(), user.getEmail(),
                        newAddress, user.getPhone());
                Toast.makeText(getApplicationContext(), "Change address successfully.", Toast.LENGTH_SHORT).show();
                usersDataSource.close();

                Intent i = new Intent(getApplicationContext(), UserActivity.class);
                startActivity(i);
            }
        });

        newPhoneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /* Update email in database. */
                String newPhone = newPhoneView.getText().toString();

                usersDataSource.open();
                usersDataSource.updateUser(user.getUserName(), user.getPassword(), user.getEmail(),
                        user.getAddress(), newPhone);
                Toast.makeText(getApplicationContext(), "Change phone number successfully.", Toast.LENGTH_SHORT).show();
                usersDataSource.close();

                Intent i = new Intent(getApplicationContext(), UserActivity.class);
                startActivity(i);
            }
        });


    }
}
