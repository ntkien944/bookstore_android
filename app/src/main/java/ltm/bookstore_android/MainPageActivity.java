package ltm.bookstore_android;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;

import ltm.bookstore_android.controller.MainPageController;
import ltm.bookstore_android.controller.SimpleBookListAdapter;
import ltm.bookstore_android.model.pojo.Book;
import ltm.bookstore_android.model.pojo.Category;


import android.support.v4.app.FragmentActivity;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;
import static ltm.bookstore_android.ulti.PublicConfig.TAG_ACTIVITY;
import static ltm.bookstore_android.ulti.PublicConfig.categoriesDataSource;
public class MainPageActivity extends FragmentActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        int categoryId = -1;
        Category category = null;

        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            String categoryIdString = extras.getString("passCategoryId");
            categoryId = Integer.parseInt(categoryIdString);
        }
        else {
            Log.d(TAG_ACTIVITY + getClass().toString(), "Failed to get passing parameter from " +
                    "main activity");
        }


        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.layout_mainpage);





        /* Main Content*/
        final GridView basicBookListView = (GridView) findViewById(R.id.view_basic_book_list);

        // Xu li phan hien ra TYPE BOOK LIST: ALL BOOKS or CATEGORY.
        TextView mCategoryListType = (TextView) findViewById(R.id.mainpage_category_name);
        if (categoryId == -1)
        {
            mCategoryListType.setText("All books");
        }
        else {
            categoriesDataSource.open();
            Category categoryTypeToShow = categoriesDataSource.getCategory(categoryId);
            categoriesDataSource.close();

            mCategoryListType.setText(categoryTypeToShow.getCategoryName());
        }


        SimpleBookListAdapter simpleBookListAdapter = MainPageController.getSimpleBookListAdapter(this, categoryId);
        basicBookListView.setAdapter(simpleBookListAdapter);
        basicBookListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Pass book id cho activity tiep theo
                Book book = (Book) basicBookListView.getItemAtPosition(position);
                int bookId = book.getBookId();
                Intent i = new Intent(getApplication(), BookPageActivity.class);
                i.putExtra("passBookId", Integer.toString(bookId));
                startActivity(i);
            }
        });
        ImageButton buttonCart = (ImageButton) findViewById(R.id.mainpage_button_cart);
        buttonCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplication(), CartActivity.class);
                startActivity(i);
            }
        });

        ImageButton mUserButtonView = (ImageButton) findViewById(R.id.user_button);
        mUserButtonView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplication(), UserActivity.class);
                startActivity(i);
            }
        });

        /* End main content. */



        final ListView mDrawerList = (ListView) findViewById(R.id.left_drawer);
        List<Category> categoryList = null;
        // Get category;
        categoriesDataSource.open();
        categoryList = categoriesDataSource.getAllCategories();
        categoriesDataSource.close();

        /* Tmp code*/
        Category allBooksCategory =new Category();
        allBooksCategory.setCategoryName("All books");
        allBooksCategory.setCategoryId(Category.CATEGORY_ID_ALL_BOOKS);
        categoryList.add(0, allBooksCategory);
        
        /***** Tmp code****/

        // Set the adapter for the list view

        ArrayAdapter<Category> arrayAdapter = new ArrayAdapter<Category>(this, android.R.layout.simple_list_item_1,
                android.R.id.text1, categoryList);
        mDrawerList.setAdapter(arrayAdapter);


        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Pass category id cho activity tiep theo
                Category category = (Category) mDrawerList.getItemAtPosition(position);
                int categoryId = category.getCategoryId();
                Intent i = new Intent(getApplication(), MainPageActivity.class);
                i.putExtra("passCategoryId", Integer.toString(categoryId));
                startActivity(i);


            }
        });


        /* End navigation drawer */


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
