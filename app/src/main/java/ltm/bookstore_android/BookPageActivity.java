package ltm.bookstore_android;

import android.app.Activity;
import android.content.Intent;
import android.database.sqlite.SQLiteException;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;


import ltm.bookstore_android.model.pojo.Book;
import ltm.bookstore_android.model.pojo.Cart;
import ltm.bookstore_android.model.pojo.Category;
import ltm.bookstore_android.ulti.PublicInfo;

import java.util.List;
import static ltm.bookstore_android.ulti.PublicConfig.*;

/**
 * Created by Administrator on 6/21/2015.
 */
public class BookPageActivity extends Activity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        int bookId = -1;
        Book book = null;

        // Lay book id tu main activity
        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            String bookIdString = extras.getString("passBookId");
            bookId = Integer.parseInt(bookIdString);
        }
        else {
            Log.d(TAG_ACTIVITY + getClass().toString(), "Failed to get passing parameter from " +
                    "main activity");
        }

        setContentView(R.layout.layout_book_page);

        // Open database to get book info
        booksDataSource.open();
        try {
            book = booksDataSource.getBook(bookId);
        } catch (SQLiteException e) {
            Log.d(TAG_SQLITE + getClass().toString(), "Cannot get book info");
        }
        booksDataSource.close();

        ImageView bookImage = (ImageView) findViewById(R.id.bookpage_image);
        TextView bookName = (TextView) findViewById(R.id.bookpage_name);
        TextView bookAuthor = (TextView) findViewById(R.id.bookpage_author);
        TextView bookPublisher = (TextView) findViewById(R.id.bookpage_publisher);
        TextView bookCategory = (TextView) findViewById(R.id.bookpage_category);
        TextView bookPrice = (TextView) findViewById(R.id.bookpage_price);


        /**
         * TODO: Download hinh cho bookImage.
         */

        bookName.setText(book.getBookName());
        bookAuthor.setText(book.getBookAuthor());
        bookPublisher.setText(book.getBookPublisher());

        // Translate bookCategoryId -> bookCategoryName
        categoriesDataSource.open();
        Category category = categoriesDataSource.getCategory(book.getBookCategoryId());
        categoriesDataSource.close();

        bookCategory.setText(category.getCategoryName());
        bookPrice.setText(Integer.toString(book.getBookPrice()));

        /* Implement back to store button */
        Button buttonBackToStore = (Button) findViewById(R.id.bookpage_button_back_to_store);
        buttonBackToStore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), MainPageActivity.class);
                startActivity(i);
            }
        });

        /**
         *  TODO: Implement buy now button
         */


        /* Implement add to cart button. */
        /**
         * TODO: CHECK da add to cart rui thi phai doi ten nut
         */
        final Button buttonAddToCart = (Button) findViewById(R.id.bookpage_button_add_to_cart);

        cartsDataSource.open();
        List<Cart> cartList = cartsDataSource.getALlCartsOfUser(PublicInfo.USERNAME);
        cartsDataSource.close();

        for (Cart cart: cartList) {
            if(cart.getBookId() == bookId) {
                buttonAddToCart.setText("Added");
            }
        }
        final int finalBookId = bookId;
        buttonAddToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonAddToCart.setText("Added");
                cartsDataSource.open();
                cartsDataSource.createCart(PublicInfo.USERNAME, finalBookId);
                cartsDataSource.close();
            }
        });


    }
}
