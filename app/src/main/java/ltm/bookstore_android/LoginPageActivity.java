package ltm.bookstore_android;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import static ltm.bookstore_android.ulti.PublicInfo.*;

import static ltm.bookstore_android.ulti.PublicConfig.*;

/**
 * Created by Administrator on 6/24/2015.
 */

/**
 * TODO: use Callback for better code.
 */
public class LoginPageActivity extends Activity  {

    private static final String[] DUMMY_CREDENTIALS = new String[]{
            "foo@example.com", "bar@example.com:world"
    };

    private UserLoginTask mAuthLoginTask = null;
    private UserRegisterTask mAuthRegisterTask = null;

    private AutoCompleteTextView mUsernameView;
    private EditText mPasswordView;
    private View mLoginFormView;
    private View mProgressView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // Set up the login form.
        mUsernameView = (AutoCompleteTextView) findViewById(R.id.email);



        mPasswordView = (EditText) findViewById(R.id.password);

        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int id, KeyEvent event) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }

                return false;
            }
        });

        Button mUsernameSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mUsernameSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attemptLogin();
            }
        });

        Button mRegisterButton = (Button) findViewById(R.id.register_button);
        mRegisterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doRegister();
            }
        });

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);


    }

    private void doRegister() {
        if(mAuthRegisterTask != null) {
            return;
        }

        // Reset errors
        mUsernameView.setError(null);
        mPasswordView.setError(null);

        //Store values at the time of the login attempt.
        String email = mUsernameView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user enterred one.
        if(!TextUtils.isEmpty(password) && !isPasswordValid(password)){
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        //Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mUsernameView.setError(getString(R.string.error_field_required));
            focusView = mUsernameView;
            cancel = true;
        } else if (!isEmailValid(email)) {
            mUsernameView.setError(getString(R.string.error_invalid_email));
            focusView = mUsernameView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attampt login and focus the first
            // form field with an error
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);

            mAuthRegisterTask = new UserRegisterTask(email, password);


            mAuthRegisterTask.execute((Void) null);
        }
    }


    private void attemptLogin() {
        if(mAuthLoginTask != null) {
            return;
        }

        // Reset errors
        mUsernameView.setError(null);
        mPasswordView.setError(null);

        //Store values at the time of the login attempt.
        String email = mUsernameView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;
        
        // Check for a valid password, if the user enterred one.
        if(!TextUtils.isEmpty(password) && !isPasswordValid(password)){
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        //Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mUsernameView.setError(getString(R.string.error_field_required));
            focusView = mUsernameView;
            cancel = true;
        } else if (!isEmailValid(email)) {
            mUsernameView.setError(getString(R.string.error_invalid_email));
            focusView = mUsernameView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attampt login and focus the first
            // form field with an error
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);

            mAuthLoginTask = new UserLoginTask(email, password);


            mAuthLoginTask.execute((Void) null);
        }

    }



    private void showProgress(final boolean show) {
        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

        mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        mLoginFormView.animate().setDuration(shortAnimTime).alpha(
            show ? 0 : 1).setListener(new AnimatorListenerAdapter() {

            @Override
            public void onAnimationEnd(Animator animation) {
                mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            }
        });

        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        mProgressView.animate().setDuration(shortAnimTime).alpha(show ? 1: 0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
            }
        });


    }

    private boolean isEmailValid(String email) {
        /**
         * TODO: Implement username rules
         */
        boolean ret = false;

        ret = true;
        //email.equals("00000000000");

        return ret;
    }

    private boolean isPasswordValid(String password) {


        return password.length() >= getResources().getInteger(R.integer.PASSWORD_MIN_LENGTH);
    }




    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {

        private final String mUsername;
        private final String mPassword;

        UserLoginTask(String email, String password) {
            mUsername = email;
            mPassword = password;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            // TODO: attempt authentication against a network service.

            try {
                // Simulate network access.
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                return false;
            }

            usersDataSource.open();
            boolean isContainThisUser = usersDataSource.isContain(mUsername,mPassword);
            usersDataSource.close();

            return isContainThisUser;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthLoginTask = null;
            showProgress(false);

            if (success) {
                /**
                  * TODO: xu li dang nhap thanh cong
                  */
                USERNAME = mUsername;
                Toast.makeText(getApplicationContext(), "Login successfully.", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(getApplicationContext(), MainPageActivity.class);
                startActivity(i);

                finish();
            } else {
                mPasswordView.setError(getString(R.string.error_incorrect_password));
                mPasswordView.requestFocus();
            }
        }

        @Override
        protected void onCancelled() {
            mAuthLoginTask = null;
            showProgress(false);
        }
    }

    public class UserRegisterTask extends AsyncTask<Void, Void, Boolean> {

        private final String mUsername;
        private final String mPassword;

        UserRegisterTask(String email, String password) {
            mUsername = email;
            mPassword = password;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            // TODO: attempt authentication against a network service.

            try {
                // Simulate network access.
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                return false;
            }

            /* Connecting to database*/


            usersDataSource.open();
            boolean isContainThisUser = usersDataSource.isContain(mUsername);
            usersDataSource.close();

            if(!isContainThisUser) { // If username not exist.
                return true;
            }
            return false;

        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthRegisterTask = null;
            showProgress(false);

            if (success) {

                /**
                 * Xu li register thanh cong
                 */
                // TODO: day data tu sqlite len server.
                usersDataSource.open();
                usersDataSource.createUser(mUsername, mPassword, "Unknown", "Unknown", "Unknown");
                usersDataSource.close();



                USERNAME = mUsername;
                Toast.makeText(getApplicationContext(), "Create user successfully.", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(getApplicationContext(), MainPageActivity.class);
                startActivity(i);

            } else {
                mPasswordView.setError(getString(R.string.error_username_exist));
                mPasswordView.requestFocus();
            }
        }

        @Override
        protected void onCancelled() {
            mAuthRegisterTask = null;
            showProgress(false);
        }
    }
}
