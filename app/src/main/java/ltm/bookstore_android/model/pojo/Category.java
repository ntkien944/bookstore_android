package ltm.bookstore_android.model.pojo;

/**
 * Created by Administrator on 6/19/2015.
 */
public class Category {
    private int categoryId;
    private String categoryName;

    public static final int CATEGORY_ID_ALL_BOOKS = -1;

    public Category() {   }
    public Category(int categoryId, String categoryName) {
        setCategoryId(categoryId);
        setCategoryName(categoryName);
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    @Override
    public String toString() {
        return getCategoryName();
    }
}
