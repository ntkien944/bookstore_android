package ltm.bookstore_android.model.pojo;


/**
 * Created by Administrator on 6/19/2015.
 */
public class Book {

    private int bookId;
    private String bookName;
    private String bookAuthor;
    private String bookPublisher;
    private int bookCategoryId;
    private int bookPrice;
    private String bookUrl;

    public String getBookUrl() {
        return bookUrl;
    }

    public void setBookUrl(String bookUrl) {
        this.bookUrl = bookUrl;
    }



    public int getBookId() {
        return bookId;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public String getBookAuthor() {
        return bookAuthor;
    }

    public void setBookAuthor(String bookAuthor) {
        this.bookAuthor = bookAuthor;
    }

    public String getBookPublisher() {
        return bookPublisher;
    }

    public void setBookPublisher(String bookPublisher) {
        this.bookPublisher = bookPublisher;
    }

    public int getBookCategoryId() {
        return bookCategoryId;
    }

    public void setBookCategoryId(int bookCategoryId) {
        this.bookCategoryId = bookCategoryId;
    }

    public int getBookPrice() {
        return bookPrice;
    }

    public void setBookPrice(int bookPrice) {
        this.bookPrice = bookPrice;
    }

    public String toString() {
        return "Book id: "
                + this.getBookId()
                + "\tBook name: "
                + this.getBookName()
                + "\tBook author: "
                + this.getBookAuthor()
                + "\tBook publisher: "
                + this.getBookPublisher()
                + "\tBook category id: "
                + "\tBook price: "
                + this.getBookPrice()
                + "\tBook url: "
                + this.getBookUrl();
    }

}
