package ltm.bookstore_android.model.pojo;

/**
 * Created by Administrator on 6/20/2015.
 */
public class Cart {
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getBookId() {
        return bookId;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

    public String toString() {
        return "User's cart: "
                + this.getUserName()
                + "\tBook id: "
                + this.getBookId();
    }

    private String userName;
    private int bookId;
}
