package ltm.bookstore_android.model.pojo;

/**
 * Created by Administrator on 6/19/2015.
 */
public class User {
    private String userName;
    private String password;
    private String email;
    private String address;
    private String phone;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String toString() {
        return "User name: "
                + this.getUserName()
                + "\tUser password: "
                + this.getPassword()
                + "\tUser email: "
                + this.getEmail()
                + "\tUser Address: "
                + this.getAddress()
                + "\tUser phone: "
                + this.getPhone();
    }



}
