package ltm.bookstore_android.controller;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import ltm.bookstore_android.R;
import ltm.bookstore_android.model.pojo.Book;

/**
 * Created by Administrator on 6/20/2015.
 */
public class SimpleBookListAdapter extends ArrayAdapter<Book>{

    private final Context context;
    private final List<Book> books;


    public SimpleBookListAdapter(Context context, List<Book> books) {
        super(context, -1, books);
        this.books = books;
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View basicBookItemView = layoutInflater.inflate(R.layout.layout_basic_book_item, parent, false);
        ImageView basicBookItemImage = (ImageView) basicBookItemView.findViewById(R.id.basic_book_image);
        TextView basicBookItemName = (TextView) basicBookItemView.findViewById(R.id.basic_book_name);
        TextView basicBookItemPrice = (TextView) basicBookItemView.findViewById(R.id.basic_book_price);



        //Set data to View
        basicBookItemImage.setImageResource(R.drawable.image_blank_book);
        basicBookItemName.setText(books.get(position).getBookName());
        String priceString = Integer.toString(books.get(position).getBookPrice());
        basicBookItemPrice.setText(priceString);

        /**
         * TODO: Download hinh cho bookImage
         */
        //new DownloadImageTask((ImageView)basicBookItemView.findViewById(R.id.basic_book_image));

        return basicBookItemView;
    }
}
