package ltm.bookstore_android.controller;

import android.content.Context;

import java.util.List;

import ltm.bookstore_android.model.pojo.Book;

import static ltm.bookstore_android.ulti.PublicConfig.booksDataSource;

/**
 * Created by Administrator on 6/21/2015.
 */
public class MainPageController {

    public static SimpleBookListAdapter getSimpleBookListAdapter(Context context, int categoryId) {
        booksDataSource.open();
        List<Book> listBook = booksDataSource.getAllBooksInCategory(categoryId);
        booksDataSource.close();

        SimpleBookListAdapter simpleBookListAdapter = new SimpleBookListAdapter(context, listBook);
        return simpleBookListAdapter;
    }
}
