create table cart (
	username varchar(30),
    book_id int,
    primary key(username, book_id),
    foreign key(username) references user(username),
    foreign key(book_id) references book(book_id)
)