create table book (
	book_id int,
    book_name varchar(50),
    book_author varchar(30),
    book_publisher varchar(30),
    category_id int,
    book_price int,
    book_url varchar(400),
    primary key(book_id),
    foreign key(category_id) references category(category_id)
)